import pathlib
import re
import sys
import tempfile


PATH = pathlib.Path(__file__).parent


path = PATH.parent / "rplugin/python"
if str(path) not in sys.path:
    sys.path.append(str(path))

import _python_execute


_ = r"\.[a-zA-Z_]\w* = .+\n"
ATTRIBUTES_PATTERN = rf"({_})+"
RANKED_ATTRIBUTES_PATTERN = rf"(\s*\d+\s+{_})+(...\n)?"

_ = r"[a-zA-Z_]\w*([:=].*)?\n"
SIGNATURE_PATTERN = rf"\(\n((\s*{_})*|\s*\?\n)\)\n"
RANKED_SIGNATURE_PATTERN = rf"\(\n((\s*\d+\s+{_})*(\s*...\n)?|\s*\?\n)\)\n"

ADDRESS_PATTERN = r"0x[0-9a-f]+"
PATH_PATTERN = r"/(.*[^/])?"


def test_trivial(capsys):
    code_string = """
pass
"""
    _test(code_string, 0, 0, 0, 1, "n", "", "\n", capsys)
    _test(code_string, 0, 3, 0, 4, "n", "", "\n", capsys)
    _test(code_string, 0, 1, 0, 1, "i", "", "\n", capsys)
    _test(code_string, 0, 4, 0, 4, "i", "", "\n", capsys)
    _test(code_string, 0, 0, 0, 1, "v", "", "\n", capsys)
    _test(code_string, 0, 3, 0, 4, "v", "", "\n", capsys)


def test_part_selection(capsys):
    code_string = """
1 + (2 - (3 * 4) / 5) ** 6
"""

    _test(code_string, 0, 0, 0, 1, "n", "", f"{1}\n\n", capsys)
    _test(code_string, 0, 2, 0, 3, "n", "", f"{1 + (2 - (3 * 4) / 5) ** 6}\n\n", capsys)
    _test(code_string, 0, 4, 0, 5, "n", "", f"{(2 - (3 * 4) / 5)}\n\n", capsys)
    _test(code_string, 0, 5, 0, 6, "n", "", f"{2}\n\n", capsys)
    _test(code_string, 0, 7, 0, 8, "n", "", f"{2 - (3 * 4) / 5}\n\n", capsys)
    _test(code_string, 0, 9, 0, 10, "n", "", f"{(3 * 4)}\n\n", capsys)
    _test(code_string, 0, 10, 0, 11, "n", "", f"{3}\n\n", capsys)
    _test(code_string, 0, 12, 0, 13, "n", "", f"{(3 * 4)}\n\n", capsys)
    _test(code_string, 0, 14, 0, 15, "n", "", f"{4}\n\n", capsys)
    _test(code_string, 0, 15, 0, 16, "n", "", f"{(3 * 4)}\n\n", capsys)
    _test(code_string, 0, 17, 0, 18, "n", "", f"{(3 * 4) / 5}\n\n", capsys)
    _test(code_string, 0, 19, 0, 20, "n", "", f"{5}\n\n", capsys)
    _test(code_string, 0, 20, 0, 21, "n", "", f"{(2 - (3 * 4) / 5)}\n\n", capsys)
    _test(code_string, 0, 22, 0, 23, "n", "", f"{(2 - (3 * 4) / 5) ** 6}\n\n", capsys)
    _test(code_string, 0, 23, 0, 24, "n", "", f"{(2 - (3 * 4) / 5) ** 6}\n\n", capsys)
    _test(code_string, 0, 25, 0, 26, "n", "", f"{6}\n\n", capsys)

    _test(code_string, 0, 1, 0, 1, "i", "", f"{1}\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"{1 + (2 - (3 * 4) / 5) ** 6}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 3, 0, 3, "i", "", _v_, capsys)
    _v_ = f"{(2 - (3 * 4) / 5)}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 5, 0, 5, "i", "", _v_, capsys)
    _test(code_string, 0, 6, 0, 6, "i", "", f"{2}\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"{2 - (3 * 4) / 5}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 8, 0, 8, "i", "", _v_, capsys)
    _v_ = f"{(3 * 4)}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 10, 0, 10, "i", "", _v_, capsys)
    _test(code_string, 0, 11, 0, 11, "i", "", f"{3}\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"{(3 * 4)}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 13, 0, 13, "i", "", _v_, capsys)
    _test(code_string, 0, 15, 0, 15, "i", "", f"{4}\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"{(3 * 4)}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 16, 0, 16, "i", "", _v_, capsys)
    _v_ = f"{(3 * 4) / 5}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 18, 0, 18, "i", "", _v_, capsys)
    _test(code_string, 0, 20, 0, 20, "i", "", f"{5}\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"{(2 - (3 * 4) / 5)}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 21, 0, 21, "i", "", _v_, capsys)
    _v_ = f"{(2 - (3 * 4) / 5) ** 6}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 23, 0, 23, "i", "", _v_, capsys)
    _v_ = f"{(2 - (3 * 4) / 5) ** 6}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 24, 0, 24, "i", "", _v_, capsys)
    _test(code_string, 0, 26, 0, 26, "i", "", f"{6}\n{ATTRIBUTES_PATTERN}\n", capsys)

    _v_ = [
        (0, 1),
        (0, 26),
        (4, 21),
        (4, 26),
        (5, 6),
        (9, 16),
        (9, 20),
        (10, 11),
        (14, 15),
        (19, 20),
        (25, 26),
    ]
    ranges = sorted(_v_, key=lambda tuple: tuple[1] - tuple[0])

    for start_column in range(len(code_string) - 2):
        for end_column in range(start_column + 1, len(code_string) - 2 + 1):
            for start_index, stop_index in ranges:
                if start_index <= start_column and end_column <= stop_index:
                    break

            else:
                raise Exception

            _v_ = f"{eval(code_string[1 + start_index: 1 + stop_index])}\n\n"
            _test(code_string, 0, start_column, 0, end_column, "v", "", _v_, capsys)


def test_attribute(capsys):
    code_string = """
float.as_integer_ratio
"""
    _ = "<method 'as_integer_ratio' of 'float' objects>"

    _test(code_string, 0, 4, 0, 5, "n", "", "<class 'float'>\n\n", capsys)
    _test(code_string, 0, 5, 0, 6, "n", "", f"{_}\n\n", capsys)
    _test(code_string, 0, 6, 0, 7, "n", "", f"{_}\n\n", capsys)

    _v_ = f"<class 'float'>\n{ATTRIBUTES_PATTERN}{SIGNATURE_PATTERN}\n"
    _test(code_string, 0, 5, 0, 5, "i", "", _v_, capsys)
    _v_ = f"<class 'float'>\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 6, 0, 6, "i", "", _v_, capsys)
    _v_ = f"<class 'float'>\n{RANKED_ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 7, 0, 7, "i", "", _v_, capsys)
    _v_ = f"<class 'float'>\n{RANKED_ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 21, 0, 22, "i", "", _v_, capsys)
    _test(code_string, 0, 22, 0, 22, "i", "", f"{_}\n{SIGNATURE_PATTERN}\n", capsys)


def test_parameters(capsys):
    code_string = """
def f(arg, *args, kwarg=(1 + 2), **kwargs):
    pass
"""

    _test(code_string, 0, 0, 0, 1, "n", "", "\n", capsys)
    _v_ = f"<function f at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 4, 0, 5, "n", "", _v_, capsys)
    _test(code_string, 0, 5, 0, 6, "n", "", "\n", capsys)
    _test(code_string, 0, 6, 0, 7, "n", "", "\n", capsys)
    _test(code_string, 0, 9, 0, 10, "n", "", "\n", capsys)
    _test(code_string, 0, 11, 0, 12, "n", "", "\n", capsys)
    _test(code_string, 0, 12, 0, 13, "n", "", "\n", capsys)
    _test(code_string, 0, 16, 0, 17, "n", "", "\n", capsys)
    _test(code_string, 0, 18, 0, 19, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 23, 0, 24, "n", "", "\n", capsys)
    _test(code_string, 0, 24, 0, 25, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 25, 0, 26, "n", "", "1\n\n", capsys)
    _test(code_string, 0, 31, 0, 32, "n", "", "\n", capsys)
    _test(code_string, 0, 33, 0, 34, "n", "", "\n", capsys)
    _test(code_string, 0, 35, 0, 36, "n", "", "\n", capsys)
    _test(code_string, 0, 41, 0, 42, "n", "", "\n", capsys)

    _test(code_string, 0, 1, 0, 1, "i", "", "\n", capsys)
    _test(code_string, 0, 5, 0, 5, "i", "", "\n", capsys)
    _test(code_string, 0, 6, 0, 6, "i", "", "\n", capsys)
    _test(code_string, 0, 7, 0, 7, "i", "", "\n", capsys)
    _test(code_string, 0, 9, 0, 9, "i", "", "\n", capsys)
    _test(code_string, 0, 10, 0, 10, "i", "", "\n", capsys)
    _test(code_string, 0, 12, 0, 12, "i", "", "\n", capsys)
    _test(code_string, 0, 13, 0, 13, "i", "", "\n", capsys)
    _test(code_string, 0, 16, 0, 16, "i", "", "\n", capsys)
    _test(code_string, 0, 19, 0, 19, "i", "", "\n", capsys)
    _test(code_string, 0, 23, 0, 23, "i", "", "\n", capsys)
    _test(code_string, 0, 24, 0, 24, "i", "", "\n", capsys)
    _test(code_string, 0, 26, 0, 26, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 31, 0, 31, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 34, 0, 34, "i", "", "\n", capsys)
    _test(code_string, 0, 35, 0, 35, "i", "", "\n", capsys)
    _test(code_string, 0, 36, 0, 36, "i", "", "\n", capsys)
    _test(code_string, 0, 41, 0, 41, "i", "", "\n", capsys)
    _test(code_string, 0, 43, 0, 43, "i", "", "\n", capsys)


def test_arguments(capsys):
    code_string = """
_ = lambda a, b, c=1 + 2, d=3 + 4: a + b + c + d
_((1 + 2), *((3 + 4),), c=(5 + 6), **dict(d=(7 + 8)))
"""

    _v_ = f"<function <lambda> at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 1, 0, 1, 1, "n", "", _v_, capsys)
    _test(code_string, 1, 1, 1, 2, "n", "", "36\n\n", capsys)
    _test(code_string, 1, 2, 1, 3, "n", "", "3\n\n", capsys)
    _test(code_string, 1, 3, 1, 4, "n", "", "1\n\n", capsys)
    _test(code_string, 1, 9, 1, 10, "n", "", "36\n\n", capsys)
    _test(code_string, 1, 11, 1, 12, "n", "", "36\n\n", capsys)
    _test(code_string, 1, 12, 1, 13, "n", "", re.escape("(7,)\n\n"), capsys)
    _test(code_string, 1, 13, 1, 14, "n", "", "7\n\n", capsys)
    _test(code_string, 1, 14, 1, 15, "n", "", "3\n\n", capsys)
    _test(code_string, 1, 24, 1, 25, "n", "", "11\n\n", capsys)
    _test(code_string, 1, 25, 1, 26, "n", "", "36\n\n", capsys)
    _test(code_string, 1, 26, 1, 27, "n", "", "11\n\n", capsys)
    _test(code_string, 1, 27, 1, 28, "n", "", "5\n\n", capsys)
    _test(code_string, 1, 35, 1, 36, "n", "", "36\n\n", capsys)
    _test(code_string, 1, 36, 1, 37, "n", "", "36\n\n", capsys)
    _test(code_string, 1, 37, 1, 38, "n", "", "<class 'dict'>\n\n", capsys)
    _test(code_string, 1, 41, 1, 42, "n", "", "{'d': 15}\n\n", capsys)
    _test(code_string, 1, 45, 1, 46, "n", "", "7\n\n", capsys)

    _ = f"<function <lambda> at {ADDRESS_PATTERN}>\n{SIGNATURE_PATTERN}\n"
    _test(code_string, 1, 1, 1, 1, "i", "", _, capsys)
    _test(code_string, 1, 2, 1, 2, "i", "", _, capsys)
    _test(code_string, 1, 4, 1, 4, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 9, 1, 9, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 10, 1, 10, "i", "", _, capsys)
    _test(code_string, 1, 12, 1, 12, "i", "", _, capsys)
    _test(code_string, 1, 15, 1, 15, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 20, 1, 20, "i", "", f"7\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = rf"\(7,\)\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 1, 22, 1, 22, "i", "", _v_, capsys)
    _v_ = f"<function <lambda> at {ADDRESS_PATTERN}>\n{RANKED_SIGNATURE_PATTERN}\n"
    _test(code_string, 1, 25, 1, 25, "i", "", _v_, capsys)
    _v_ = f"<function <lambda> at {ADDRESS_PATTERN}>\n{RANKED_SIGNATURE_PATTERN}\n"
    _test(code_string, 1, 26, 1, 26, "i", "", _v_, capsys)
    _test(code_string, 1, 28, 1, 28, "i", "", f"5\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 33, 1, 33, "i", "", f"11\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 36, 1, 36, "i", "", _, capsys)
    _test(code_string, 1, 37, 1, 37, "i", "", _, capsys)
    _v_ = f"<class 'dict'>\n{ATTRIBUTES_PATTERN}{SIGNATURE_PATTERN}\n"
    _test(code_string, 1, 38, 1, 38, "i", "", _v_, capsys)
    _v_ = f"<class 'dict'>\n{SIGNATURE_PATTERN}\n"
    _test(code_string, 1, 42, 1, 42, "i", "", _v_, capsys)
    _test(code_string, 1, 46, 1, 46, "i", "", f"7\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"{{'d': 15}}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 1, 52, 1, 52, "i", "", _v_, capsys)
    _test(code_string, 1, 53, 1, 53, "i", "", f"36\n{ATTRIBUTES_PATTERN}\n", capsys)


def test_assignment_name(capsys):
    code_string = """
name = 1 + 2
name = (3 + 4)
"""
    _test(code_string, 1, 0, 1, 1, "n", "", "7\n\n", capsys)
    _test(code_string, 1, 5, 1, 6, "n", "", "\n", capsys)
    _test(code_string, 1, 1, 1, 1, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 4, 1, 4, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 6, 1, 6, "i", "", "\n", capsys)


def test_assignment_attribute(capsys):
    code_string = """
class C: attr = 1 + 2
C.attr = (3 + 4)
"""

    _test(code_string, 1, 0, 1, 1, "n", "", "<class 'C'>\n\n", capsys)
    _test(code_string, 1, 1, 1, 2, "n", "", "7\n\n", capsys)
    _test(code_string, 1, 2, 1, 3, "n", "", "7\n\n", capsys)
    _test(code_string, 1, 7, 1, 8, "n", "", "\n", capsys)

    _v_ = f"<class 'C'>\n{ATTRIBUTES_PATTERN}{SIGNATURE_PATTERN}\n"
    _test(code_string, 1, 1, 1, 1, "i", "", _v_, capsys)
    _v_ = f"<class 'C'>\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 1, 2, 1, 2, "i", "", _v_, capsys)
    _v_ = f"<class 'C'>\n{RANKED_ATTRIBUTES_PATTERN}\n"
    _test(code_string, 1, 3, 1, 3, "i", "", _v_, capsys)
    _test(code_string, 1, 6, 1, 6, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 1, 8, 1, 8, "i", "", "\n", capsys)


def test_assignment_tuple(capsys):
    code_string = """
name = 1 + 2; _ = 3 + 4
class C: attr = 5 + 6
name, (_, C.attr) = (7 + 8), ((9 + 10), (11 + 12))
"""

    _test(code_string, 2, 0, 2, 1, "n", "", "15\n\n", capsys)
    _test(code_string, 2, 4, 2, 5, "n", "", "\n", capsys)
    _test(code_string, 2, 6, 2, 7, "n", "", "\n", capsys)
    _test(code_string, 2, 7, 2, 8, "n", "", "19\n\n", capsys)
    _test(code_string, 2, 8, 2, 9, "n", "", "\n", capsys)
    _test(code_string, 2, 10, 2, 11, "n", "", "<class 'C'>\n\n", capsys)
    _test(code_string, 2, 11, 2, 12, "n", "", "23\n\n", capsys)
    _test(code_string, 2, 12, 2, 13, "n", "", "23\n\n", capsys)
    _test(code_string, 2, 16, 2, 17, "n", "", "\n", capsys)
    _test(code_string, 2, 18, 2, 19, "n", "", "\n", capsys)

    _test(code_string, 2, 1, 2, 1, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 2, 4, 2, 4, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 2, 5, 2, 5, "i", "", "\n", capsys)
    _test(code_string, 2, 7, 2, 7, "i", "", "\n", capsys)
    _test(code_string, 2, 8, 2, 8, "i", "", f"7\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 2, 9, 2, 9, "i", "", "\n", capsys)
    _v_ = f"<class 'C'>\n{ATTRIBUTES_PATTERN}{SIGNATURE_PATTERN}\n"
    _test(code_string, 2, 11, 2, 11, "i", "", _v_, capsys)
    _v_ = f"<class 'C'>\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 2, 12, 2, 12, "i", "", _v_, capsys)
    _v_ = f"<class 'C'>\n{RANKED_ATTRIBUTES_PATTERN}\n"
    _test(code_string, 2, 13, 2, 13, "i", "", _v_, capsys)
    _test(code_string, 2, 16, 2, 16, "i", "", f"11\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 2, 17, 2, 17, "i", "", "\n", capsys)
    _test(code_string, 2, 19, 2, 19, "i", "", "\n", capsys)


def test_import(capsys):
    code_string = """
import urllib.parse
"""

    _test(code_string, 0, 0, 0, 1, "n", "", "\n", capsys)
    _v_ = f"<module 'urllib' from '{PATH_PATTERN}'>\n\n"
    _test(code_string, 0, 7, 0, 8, "n", "", _v_, capsys)
    _v_ = f"<module 'urllib.parse' from '{PATH_PATTERN}>\n\n"
    _test(code_string, 0, 13, 0, 14, "n", "", _v_, capsys)
    _v_ = f"<module 'urllib.parse' from '{PATH_PATTERN}>\n\n"
    _test(code_string, 0, 14, 0, 15, "n", "", _v_, capsys)

    _test(code_string, 0, 6, 0, 6, "i", "", "\n", capsys)
    _test(code_string, 0, 7, 0, 7, "i", "", "\n", capsys)
    _test(code_string, 0, 13, 0, 13, "i", "", "\n", capsys)
    _test(code_string, 0, 14, 0, 14, "i", "", "\n", capsys)
    _test(code_string, 0, 19, 0, 19, "i", "", "\n", capsys)


def test_import_as(capsys):
    code_string = """
import urllib.parse as u_parse
"""

    _test(code_string, 0, 0, 0, 1, "n", "", "\n", capsys)
    _test(code_string, 0, 7, 0, 8, "n", "", "\n", capsys)
    _test(code_string, 0, 13, 0, 14, "n", "", "\n", capsys)
    _test(code_string, 0, 14, 0, 15, "n", "", "\n", capsys)
    _test(code_string, 0, 20, 0, 21, "n", "", "\n", capsys)
    _v_ = f"<module 'urllib.parse' from '{PATH_PATTERN}>\n\n"
    _test(code_string, 0, 23, 0, 24, "n", "", _v_, capsys)

    _test(code_string, 0, 6, 0, 6, "i", "", "\n", capsys)
    _test(code_string, 0, 7, 0, 7, "i", "", "\n", capsys)
    _test(code_string, 0, 13, 0, 13, "i", "", "\n", capsys)
    _test(code_string, 0, 14, 0, 14, "i", "", "\n", capsys)
    _test(code_string, 0, 19, 0, 19, "i", "", "\n", capsys)
    _test(code_string, 0, 22, 0, 22, "i", "", "\n", capsys)
    _test(code_string, 0, 30, 0, 30, "i", "", "\n", capsys)


def test_from_import_star(capsys):
    code_string = """
from urllib.parse import *
"""

    _test(code_string, 0, 0, 0, 1, "n", "", "\n", capsys)
    _test(code_string, 0, 5, 0, 6, "n", "", "\n", capsys)
    _test(code_string, 0, 11, 0, 12, "n", "", "\n", capsys)
    _test(code_string, 0, 12, 0, 13, "n", "", "\n", capsys)
    _test(code_string, 0, 18, 0, 19, "n", "", "\n", capsys)
    _test(code_string, 0, 25, 0, 26, "n", "", "\n", capsys)

    _test(code_string, 0, 4, 0, 4, "i", "", "\n", capsys)
    _test(code_string, 0, 11, 0, 11, "i", "", "\n", capsys)
    _test(code_string, 0, 12, 0, 12, "i", "", "\n", capsys)
    _test(code_string, 0, 17, 0, 17, "i", "", "\n", capsys)
    _test(code_string, 0, 24, 0, 24, "i", "", "\n", capsys)
    _test(code_string, 0, 26, 0, 26, "i", "", "\n", capsys)


def test_from_import_names(capsys):
    code_string = """
from urllib.parse import quote, unquote
"""

    _test(code_string, 0, 0, 0, 1, "n", "", "\n", capsys)
    _test(code_string, 0, 5, 0, 6, "n", "", "\n", capsys)
    _test(code_string, 0, 11, 0, 12, "n", "", "\n", capsys)
    _test(code_string, 0, 12, 0, 13, "n", "", "\n", capsys)
    _test(code_string, 0, 18, 0, 19, "n", "", "\n", capsys)
    _v_ = f"<function quote at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 25, 0, 26, "n", "", _v_, capsys)
    _test(code_string, 0, 30, 0, 31, "n", "", "\n", capsys)
    _v_ = f"<function unquote at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 32, 0, 33, "n", "", _v_, capsys)

    _test(code_string, 0, 4, 0, 4, "i", "", "\n", capsys)
    _test(code_string, 0, 11, 0, 11, "i", "", "\n", capsys)
    _test(code_string, 0, 12, 0, 12, "i", "", "\n", capsys)
    _test(code_string, 0, 17, 0, 17, "i", "", "\n", capsys)
    _test(code_string, 0, 24, 0, 24, "i", "", "\n", capsys)
    _v_ = f"<module 'urllib.parse' from '{PATH_PATTERN}>\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 25, 0, 25, "i", "", _v_, capsys)
    _v_ = f"<module 'urllib.parse' from '{PATH_PATTERN}>\n{RANKED_ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 30, 0, 30, "i", "", _v_, capsys)
    _v_ = f"<module 'urllib.parse' from '{PATH_PATTERN}>\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 31, 0, 31, "i", "", _v_, capsys)
    _v_ = f"<module 'urllib.parse' from '{PATH_PATTERN}>\n{RANKED_ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 39, 0, 39, "i", "", _v_, capsys)


def test_raise_ifexp(capsys):
    code_string = """
(1 + 2) if (3 + 4) else (5 + 6)
"""

    _test(code_string, 0, 0, 0, 1, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 8, 0, 9, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 11, 0, 12, "n", "", "7\n\n", capsys)
    _test(code_string, 0, 19, 0, 20, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 24, 0, 25, "n", "", "\n", capsys)

    _test(code_string, 0, 7, 0, 7, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"3\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 10, 0, 10, "i", "", _v_, capsys)  # not perfect
    _test(code_string, 0, 18, 0, 18, "i", "", f"7\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"3\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 23, 0, 23, "i", "", _v_, capsys)  # not perfect
    _test(code_string, 0, 31, 0, 31, "i", "", "\n", capsys)


def test_raise_lambda(capsys):
    code_string = """
    sorted([1, 2], key=lambda arg: -arg)
"""

    _ = f"<function <lambda> at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 23, 0, 24, "n", "", _, capsys)
    _test(code_string, 0, 30, 0, 31, "n", "", "\n", capsys)
    _test(code_string, 0, 33, 0, 34, "n", "", _, capsys)
    _test(code_string, 0, 35, 0, 36, "n", "", "-1\n\n", capsys)

    _ = f"<function <lambda> at {ADDRESS_PATTERN}>\n{SIGNATURE_PATTERN}\n"
    _test(code_string, 0, 29, 0, 29, "i", "", _, capsys)
    _test(code_string, 0, 33, 0, 33, "i", "", "\n", capsys)
    _test(code_string, 0, 34, 0, 34, "i", "", _, capsys)
    _test(code_string, 0, 39, 0, 39, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)


def test_raise_generator(capsys):
    _v_ = f"<generator object <genexpr> at {ADDRESS_PATTERN}>"
    _test_raise_comprehension("(", ")", _v_, capsys)


def test_raise_list_comprehension(capsys):
    _test_raise_comprehension("[", "]", re.escape("[(2, 5)]"), capsys)


def test_raise_set_comprehension(capsys):
    _test_raise_comprehension("{", "}", re.escape("{(2, 5)}"), capsys)


def _test_raise_comprehension(left_string, right_string, object_pattern, capsys):
    code_string = f"""
{left_string}((a +1), (e +2)) for a, b in enumerate([1, 2]) if (0 < a) for c, (d, e) in [((a + 3), ((b + 4), (a + b)))]{right_string}
"""

    def e(pattern_string):
        return "\n" if left_string == "(" else pattern_string

    _ = f"{object_pattern}\n\n"
    _test(code_string, 0, 0, 0, 1, "n", "", _, capsys)
    _test(code_string, 0, 1, 0, 2, "n", "", e(re.escape("(2, 5)\n\n")), capsys)
    _test(code_string, 0, 2, 0, 3, "n", "", e("2\n\n"), capsys)
    _test(code_string, 0, 3, 0, 4, "n", "", e("1\n\n"), capsys)
    _test(code_string, 0, 8, 0, 9, "n", "", e(re.escape("(2, 5)\n\n")), capsys)
    _test(code_string, 0, 18, 0, 19, "n", "", _, capsys)
    _test(code_string, 0, 22, 0, 23, "n", "", e("0\n\n"), capsys)
    _test(code_string, 0, 23, 0, 24, "n", "", _, capsys)
    _test(code_string, 0, 25, 0, 26, "n", "", e("1\n\n"), capsys)
    _test(code_string, 0, 27, 0, 28, "n", "", _, capsys)
    _test(code_string, 0, 30, 0, 31, "n", "", "<class 'enumerate'>\n\n", capsys)
    _v_ = f"<enumerate object at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 39, 0, 40, "n", "", _v_, capsys)
    _test(code_string, 0, 40, 0, 41, "n", "", re.escape("[1, 2]\n\n"), capsys)
    _test(code_string, 0, 41, 0, 42, "n", "", "1\n\n", capsys)
    _test(code_string, 0, 48, 0, 49, "n", "", _, capsys)
    _test(code_string, 0, 51, 0, 52, "n", "", e("False\n\n"), capsys)
    _test(code_string, 0, 56, 0, 57, "n", "", e("0\n\n"), capsys)
    _test(code_string, 0, 59, 0, 60, "n", "", _, capsys)
    _test(code_string, 0, 63, 0, 64, "n", "", e("4\n\n"), capsys)
    _test(code_string, 0, 64, 0, 65, "n", "", _, capsys)
    _test(code_string, 0, 66, 0, 67, "n", "", _, capsys)
    _test(code_string, 0, 67, 0, 68, "n", "", e("6\n\n"), capsys)
    _test(code_string, 0, 68, 0, 69, "n", "", _, capsys)
    _test(code_string, 0, 73, 0, 74, "n", "", _, capsys)
    _test(code_string, 0, 76, 0, 77, "n", "", e(re.escape("[(4, (6, 3))]\n\n")), capsys)
    _test(code_string, 0, 77, 0, 78, "n", "", e(re.escape("(4, (6, 3))\n\n")), capsys)
    _test(code_string, 0, 78, 0, 79, "n", "", e("4\n\n"), capsys)
    _test(code_string, 0, 79, 0, 80, "n", "", e("1\n\n"), capsys)
    _test(code_string, 0, 107, 0, 108, "n", "", _, capsys)

    _ = f"{object_pattern}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 1, 0, 1, "i", "", _, capsys)
    _test(code_string, 0, 4, 0, 4, "i", "", e(f"1\n{ATTRIBUTES_PATTERN}\n"), capsys)
    _v_ = e(rf"\(2, 5\)\n{ATTRIBUTES_PATTERN}\n")
    _test(code_string, 0, 9, 0, 9, "i", "", _v_, capsys)
    _v_ = e(rf"\(2, 5\)\n{ATTRIBUTES_PATTERN}\n")
    _test(code_string, 0, 17, 0, 17, "i", "", _v_, capsys)
    _test(code_string, 0, 21, 0, 21, "i", "", _, capsys)
    _test(code_string, 0, 23, 0, 23, "i", "", e(f"0\n{ATTRIBUTES_PATTERN}\n"), capsys)
    _test(code_string, 0, 24, 0, 24, "i", "", _, capsys)
    _test(code_string, 0, 29, 0, 29, "i", "", _, capsys)
    _v_ = f"<class 'enumerate'>\n{SIGNATURE_PATTERN}\n"
    _test(code_string, 0, 39, 0, 39, "i", "", _v_, capsys)
    _test(code_string, 0, 42, 0, 42, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"<enumerate object at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 47, 0, 47, "i", "", _v_, capsys)
    _test(code_string, 0, 50, 0, 50, "i", "", _, capsys)
    _test(code_string, 0, 57, 0, 57, "i", "", e(f"0\n{ATTRIBUTES_PATTERN}\n"), capsys)
    _v_ = e(f"False\n{ATTRIBUTES_PATTERN}\n")
    _test(code_string, 0, 58, 0, 58, "i", "", _v_, capsys)
    _test(code_string, 0, 62, 0, 62, "i", "", _, capsys)
    _test(code_string, 0, 64, 0, 64, "i", "", e(f"4\n{ATTRIBUTES_PATTERN}\n"), capsys)
    _test(code_string, 0, 65, 0, 65, "i", "", _, capsys)
    _test(code_string, 0, 68, 0, 68, "i", "", e(f"6\n{ATTRIBUTES_PATTERN}\n"), capsys)
    _test(code_string, 0, 72, 0, 72, "i", "", _, capsys)
    _test(code_string, 0, 75, 0, 75, "i", "", _, capsys)
    _test(code_string, 0, 85, 0, 85, "i", "", e(f"4\n{ATTRIBUTES_PATTERN}\n"), capsys)
    _v_ = e(rf"\[\(4, \(6, 3\)\)\]\n{ATTRIBUTES_PATTERN}\n")
    _test(code_string, 0, 107, 0, 107, "i", "", _v_, capsys)
    _test(code_string, 0, 108, 0, 108, "i", "", _, capsys)


def test_raise_dict_comprehension(capsys):
    code_string = """
{ (a +1): (e +2)  for a, b in enumerate([1, 2]) if (0 < a) for c, (d, e) in [((a + 3), ((b + 4), (a + b)))]}
"""

    _ = re.escape("{2: 5}\n\n")
    _test(code_string, 0, 0, 0, 1, "n", "", _, capsys)
    _test(code_string, 0, 2, 0, 3, "n", "", "2\n\n", capsys)
    _test(code_string, 0, 3, 0, 4, "n", "", "1\n\n", capsys)
    _test(code_string, 0, 8, 0, 9, "n", "", _, capsys)
    _test(code_string, 0, 10, 0, 11, "n", "", "5\n\n", capsys)
    _test(code_string, 0, 11, 0, 12, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 18, 0, 19, "n", "", _, capsys)
    _test(code_string, 0, 22, 0, 23, "n", "", "0\n\n", capsys)
    _test(code_string, 0, 23, 0, 24, "n", "", _, capsys)
    _test(code_string, 0, 25, 0, 26, "n", "", "1\n\n", capsys)
    _test(code_string, 0, 27, 0, 28, "n", "", _, capsys)
    _test(code_string, 0, 30, 0, 31, "n", "", "<class 'enumerate'>\n\n", capsys)
    _v_ = f"<enumerate object at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 39, 0, 40, "n", "", _v_, capsys)
    _test(code_string, 0, 40, 0, 41, "n", "", re.escape("[1, 2]\n\n"), capsys)
    _test(code_string, 0, 41, 0, 42, "n", "", "1\n\n", capsys)
    _test(code_string, 0, 48, 0, 49, "n", "", _, capsys)
    _test(code_string, 0, 51, 0, 52, "n", "", "False\n\n", capsys)
    _test(code_string, 0, 56, 0, 57, "n", "", "0\n\n", capsys)
    _test(code_string, 0, 59, 0, 60, "n", "", _, capsys)
    _test(code_string, 0, 63, 0, 64, "n", "", "4\n\n", capsys)
    _test(code_string, 0, 64, 0, 65, "n", "", _, capsys)
    _test(code_string, 0, 66, 0, 67, "n", "", _, capsys)
    _test(code_string, 0, 67, 0, 68, "n", "", "6\n\n", capsys)
    _test(code_string, 0, 68, 0, 69, "n", "", _, capsys)
    _test(code_string, 0, 73, 0, 74, "n", "", _, capsys)
    _test(code_string, 0, 76, 0, 77, "n", "", re.escape("[(4, (6, 3))]\n\n"), capsys)
    _test(code_string, 0, 77, 0, 78, "n", "", re.escape("(4, (6, 3))\n\n"), capsys)
    _test(code_string, 0, 78, 0, 79, "n", "", "4\n\n", capsys)
    _test(code_string, 0, 79, 0, 80, "n", "", "1\n\n", capsys)
    _test(code_string, 0, 107, 0, 108, "n", "", _, capsys)

    _ = rf"\{{2: 5\}}\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 1, 0, 1, "i", "", _, capsys)
    _test(code_string, 0, 4, 0, 4, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 8, 0, 8, "i", "", f"2\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 9, 0, 9, "i", "", _, capsys)
    _test(code_string, 0, 12, 0, 12, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 16, 0, 16, "i", "", f"5\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 21, 0, 21, "i", "", _, capsys)
    _test(code_string, 0, 23, 0, 23, "i", "", f"0\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 24, 0, 24, "i", "", _, capsys)
    _test(code_string, 0, 29, 0, 29, "i", "", _, capsys)
    _v_ = f"<class 'enumerate'>\n{SIGNATURE_PATTERN}\n"
    _test(code_string, 0, 39, 0, 39, "i", "", _v_, capsys)
    _test(code_string, 0, 42, 0, 42, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = f"<enumerate object at {ADDRESS_PATTERN}>\n\n"
    _test(code_string, 0, 47, 0, 47, "i", "", _v_, capsys)
    _test(code_string, 0, 50, 0, 50, "i", "", _, capsys)
    _test(code_string, 0, 57, 0, 57, "i", "", f"0\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 58, 0, 58, "i", "", f"False\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 62, 0, 62, "i", "", _, capsys)
    _test(code_string, 0, 64, 0, 64, "i", "", f"4\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 65, 0, 65, "i", "", _, capsys)
    _test(code_string, 0, 68, 0, 68, "i", "", f"6\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 72, 0, 72, "i", "", _, capsys)
    _test(code_string, 0, 75, 0, 75, "i", "", _, capsys)
    _test(code_string, 0, 85, 0, 85, "i", "", f"4\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = rf"\[\(4, \(6, 3\)\)\]\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 107, 0, 107, "i", "", _v_, capsys)
    _test(code_string, 0, 108, 0, 108, "i", "", _, capsys)


def test_raise_for(capsys):
    code_string = """
for a, (b, c) in [((1 + 2), ((3 + 4), (5 + 6)))]:
    print("body")
"""

    _ = "body\n"
    _test(code_string, 0, 0, 0, 1, "n", _, "\n", capsys)
    _test(code_string, 0, 4, 0, 5, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 5, 0, 6, "n", _, "\n", capsys)
    _test(code_string, 0, 7, 0, 8, "n", _, "\n", capsys)
    _test(code_string, 0, 8, 0, 9, "n", "", "7\n\n", capsys)
    _test(code_string, 0, 9, 0, 10, "n", _, "\n", capsys)
    _test(code_string, 0, 14, 0, 15, "n", _, "\n", capsys)
    _test(code_string, 0, 17, 0, 18, "n", "", re.escape("[(3, (7, 11))]\n\n"), capsys)
    _test(code_string, 0, 19, 0, 20, "n", "", "3\n\n", capsys)
    _test(code_string, 0, 48, 0, 49, "n", _, "\n", capsys)

    _test(code_string, 0, 3, 0, 3, "i", _, "\n", capsys)
    _test(code_string, 0, 5, 0, 5, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 6, 0, 6, "i", _, "\n", capsys)
    _test(code_string, 0, 8, 0, 8, "i", _, "\n", capsys)
    _test(code_string, 0, 9, 0, 9, "i", "", f"7\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 10, 0, 10, "i", _, "\n", capsys)
    _test(code_string, 0, 13, 0, 13, "i", _, "\n", capsys)
    _test(code_string, 0, 16, 0, 16, "i", _, "\n", capsys)
    _test(code_string, 0, 21, 0, 21, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 26, 0, 26, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _v_ = rf"\[\(3, \(7, 11\)\)\]\n{ATTRIBUTES_PATTERN}\n"
    _test(code_string, 0, 48, 0, 48, "i", "", _v_, capsys)
    _test(code_string, 0, 49, 0, 49, "i", _, "\n", capsys)


def test_raise_if(capsys):
    code_string = """
if (1 == 2):
    pass
elif (3 == 4):
    pass
elif (5 != 6):
    print("body")
elif (7 != 8):
    pass
else:
    pass
"""

    _ = "body\n"
    _test(code_string, 0, 0, 0, 1, "n", _, "\n", capsys)
    _test(code_string, 0, 3, 0, 4, "n", "", "False\n\n", capsys)
    _test(code_string, 0, 4, 0, 5, "n", "", "1\n\n", capsys)
    _test(code_string, 0, 11, 0, 12, "n", _, "\n", capsys)
    _test(code_string, 2, 0, 2, 1, "n", _, "\n", capsys)
    _test(code_string, 2, 5, 2, 6, "n", "", "False\n\n", capsys)
    _test(code_string, 2, 6, 2, 7, "n", "", "3\n\n", capsys)
    _test(code_string, 2, 13, 2, 14, "n", _, "\n", capsys)
    _test(code_string, 4, 0, 4, 1, "n", _, "\n", capsys)
    _test(code_string, 4, 5, 4, 6, "n", "", "True\n\n", capsys)
    _test(code_string, 4, 6, 4, 7, "n", "", "5\n\n", capsys)
    _test(code_string, 4, 13, 4, 14, "n", _, "\n", capsys)
    _test(code_string, 6, 0, 6, 1, "n", _, "\n", capsys)
    _test(code_string, 6, 5, 6, 6, "n", _, "\n", capsys)
    _test(code_string, 6, 6, 6, 7, "n", _, "\n", capsys)
    _test(code_string, 6, 13, 6, 14, "n", _, "\n", capsys)
    _test(code_string, 8, 0, 8, 1, "n", _, "\n", capsys)
    _test(code_string, 8, 4, 8, 5, "n", _, "\n", capsys)

    _test(code_string, 0, 2, 0, 2, "i", _, "\n", capsys)
    _test(code_string, 0, 5, 0, 5, "i", "", f"1\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 11, 0, 11, "i", "", f"False\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 0, 12, 0, 12, "i", _, "\n", capsys)
    _test(code_string, 2, 4, 2, 4, "i", _, "\n", capsys)
    _test(code_string, 2, 7, 2, 7, "i", "", f"3\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 2, 13, 2, 13, "i", "", f"False\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 2, 14, 2, 14, "i", _, "\n", capsys)
    _test(code_string, 4, 4, 4, 4, "i", _, "\n", capsys)
    _test(code_string, 4, 7, 4, 7, "i", "", f"5\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 4, 13, 4, 13, "i", "", f"True\n{ATTRIBUTES_PATTERN}\n", capsys)
    _test(code_string, 4, 14, 4, 14, "i", _, "\n", capsys)
    _test(code_string, 6, 4, 6, 4, "i", _, "\n", capsys)
    _test(code_string, 6, 7, 6, 7, "i", _, "\n", capsys)
    _test(code_string, 6, 13, 6, 13, "i", _, "\n", capsys)
    _test(code_string, 6, 14, 6, 14, "i", _, "\n", capsys)
    _test(code_string, 8, 4, 8, 4, "i", _, "\n", capsys)
    _test(code_string, 8, 5, 8, 5, "i", _, "\n", capsys)


def _test(
    code_string,
    start_line,
    start_column,
    end_line,
    end_column,
    mode_string,
    stdout_pattern,
    stderr_pattern,
    capsys,
):
    main = _python_execute._Main()

    with tempfile.NamedTemporaryFile() as temp_file:
        main._temp_file = temp_file

        _v_ = _python_execute._Request(
            code_string=code_string[1:-1],
            start_line=start_line,
            start_column=start_column,
            end_line=end_line,
            end_column=end_column,
            mode_string=mode_string,
            to_stderr=True,
        )
        _python_execute._process_request(_v_, dict(), main)

        capture_result = capsys.readouterr()

        try:
            _v_ = re.match(stdout_pattern + r"\Z", capture_result.out, re.MULTILINE)
            assert _v_ is not None

        except Exception:
            print("expected", repr(stdout_pattern))
            print("got")
            print(capture_result.out)
            raise

        try:
            _v_ = re.match(stderr_pattern + r"\Z", capture_result.err, re.MULTILINE)
            assert _v_ is not None

        except Exception:
            print("expected", repr(stderr_pattern))
            print("got")
            print(capture_result.err)
            raise
