# *PythonExecute*

is a plugin for [*Neovim*](https://github.com/neovim/neovim) providing an interface to Python for code execution and object introspection.
It uses [*Pynvim*](https://github.com/neovim/pynvim), [*Treesitter*](https://github.com/tree-sitter/tree-sitter) and [*LibCST*](https://github.com/Instagram/LibCST) to process code, and [*RapidFuzz*](https://github.com/maxbachmann/RapidFuzz) to rank options.

[[_TOC_]]

**Update:** the showcases represent an earlier version which doesn't use floating windows and highlights, see [2023-10-09](#2023-10-09)
![`videos/usage.m4v`](videos/usage.m4v)

Along general lines, *PythonExecute*
- starts a Python process, which
    - creates a debug session, sets a breakpoint and starts debugging
    - in a loop
        - receives a request
        - executes code and prints relevant information
- creates a window which shows the output of the process
- on command, sends a request to the process

The target use cases are interactive development similar to [*Jupyter notebook*](https://github.com/jupyter/notebook)s and simple debugging similar to print debugging.

## Features

- Execute expression/statement at cursor in Normal mode
- Execute expression/statement at cursor and introspect object in Insert mode
- Execute expression/statement containing selection in Visual mode
- Support for
    - aliases (`as`) <details><summary>Example</summary> ![`videos/aliases.m4v`](videos/aliases.m4v)</details>
    - comprehensions <details><summary>Example</summary> ![`videos/comprehensions.m4v`](videos/comprehensions.m4v)</details>
    - conditional tests (`elif`)
    - if expressions
    - imports
    - keyword arguments
    - unpacking
- Optionally start process on execute
- Optionally restart process if out of scope
- Optionally execute on cursor movement/mode change

## Quickstart

- Install [*Pipenv*](https://github.com/pypa/pipenv)
    - e.g. `python -m pip install pipenv`
- Create virtual environment
    - e.g. `python -m pipenv update`
- Install *PythonExecute*
    - e.g. `python -m pipenv run nvim -c "set rtp+=$PWD" -c UpdateRemotePlugins`
    - or
```lua
require('lazy').setup({
  {
    'notEvil/PythonExecute',
    url='https://gitlab.com/notEvil/PythonExecute',
    build=':UpdateRemotePlugins',
  }
})
```
- Start *Neovim*
    - e.g. `python -m pipenv run nvim`

## Usage

- Start process with `:StartPython`
    - `:StartPython`: use the same Python as *Neovim* and the current file
    - `:StartPython PYTHON`: use `PYTHON` and the current file
    - `:StartPython EXECUTABLE [ARG ...] [ PATH | -m MODULE ]`: use `EXECUTABLE [ARG ...]` and file at `PATH` or module `MODULE`
        - e.g. `:StartPython pipenv run python %` or `:StartPython python -m pytest -- .`
    - append ` -- [ARG ...]` to pass arguments
- Stop process with `:StopProcess`
- In Normal mode: `:ExecutePython n`
    - e.g. `vim.keymap.set('n', '<a-cr>', ':ExecutePython n<cr>')`
- In Insert mode: `:ExecutePython i`
    - e.g. `vim.keymap.set('i', '<a-cr>', '<c-o>:ExecutePython i<cr>')`
- In Visual mode: `:ExecutePython v`
    - without `'<,'>`
    - e.g. `vim.keymap.set('v', '<a-cr>', ':<c-u>ExecutePython v<cr>')`

## Configuration

```lua
vim.g.pe_auto_start = 0  -- if not 0, start process on execute
vim.g.pe_auto_scope = 0  -- if not 0, restart process if out of scope
-- vim.g.pe_auto_execute = 250  -- execute if cursor didn't move / mode didn't change [ms]
vim.g.pe_window_size = 8  -- size of output window [lines]
```

**Remarks**
- You probably shouldn't set `pe_auto_execute` (and `pe_auto_scope`) when your code potentially creates undesired side effects like delete file/directory or truncate table.

## Motivation

Over the years I developed the habit of using print statements as part of Python development, to see what I'm currently working with and to fix bugs.
*Jupyter notebook*s cover the first point, as long as you stay in the global scope.
But as soon as you write functions, a lot of its magic is lost.
Regarding the second point, using a debugger is reasonable for difficult bugs but overkill for minor/obvious bugs.

Then, in one of the episodes of [*Python Podcast*](https://python-podcast.de/) (German) a guest mentioned using a debugger for interactive development, which never crossed my mind but seemed like a good alternative to my print statement habit.

## Major updates

### 2023-10-09

![`screenshots/2023-10-09.png`](screenshots/2023-10-09.png)

- Introduced floating windows with highlights
    - enabled by default, use `:ExecutePython {mode} to_stderr=1` to switch back to the previous behaviour
    - uses syntax highlighting for Python; treesitter does a very good job and most output should look reasonable
- Improved representation of functions
    - e.g. instead of `<function {name} at {address}>` it now shows `{name}({arguments})` with defaults and type annotations if available
