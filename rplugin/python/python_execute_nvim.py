import pynvim
import os
import pathlib
import shlex
import struct
import subprocess
import sys
import threading
import traceback
import typing


PATH = pathlib.Path(__file__).parent


@pynvim.plugin
class PythonExecute:
    def __init__(self, nvim):
        super().__init__()

        self.nvim = nvim

        self._python_execute = None

        self._process = None
        self._buffer_id = None
        self._extmark = None
        self._output_buffer_id = None
        self._output_window_id = None
        self._autocmd_ids = None
        self._start_args = []

        self._auto_timer = None
        self._auto_position = None

        self._to_stderr = False
        self._window_buffer_id = None
        self._window_window_id = None
        self._window_autocmd_id = None

    @pynvim.command(
        "StartPython",
        nargs="?",  # pyright: ignore [reportGeneralTypeIssues]
    )  # fmt: skip
    def start(self, args, line_index=None):  # col: skip
        self.stop(as_error=False)

        if not self._start(args, line_index):
            self.stop()
            return False

        return True

    def _start(self, args, line_index):
        # args
        arguments = [] if len(args) == 0 else shlex.split(args[0])

        try:
            index = arguments.index("--")

        except ValueError:
            left_arguments = arguments
            right_arguments = []

        else:
            left_arguments = arguments[:index]
            right_arguments = arguments[index + 1 :]

        if len(left_arguments) == 0:
            left_arguments.append(sys.executable)

        # path
        break_path_string = self.nvim.api.buf_get_name(0)

        if break_path_string == "" or self.nvim.api.buf_get_option(0, "modified"):
            self._write_error("File is not saved")
            return False

        if len(left_arguments) == 1:
            left_arguments.append(break_path_string)

        # cursor
        _v_ = line_index is None
        _v_ = (self.nvim.api.win_get_cursor(0)[0] - 1) if _v_ else line_index
        tuple = self._get_position(_v_)
        if tuple is None:
            self._write_error("No statement at cursor")
            return False

        line_index, column_index = tuple

        _v_ = self._get_python_execute()
        _v_ = _v_.get_ranges(line_index, column_index, line_index, column_index, True)
        statement_range = _v_.get("statement")

        if statement_range is None:
            self._write_error("No statement at cursor")
            return False

        # process
        match left_arguments[-2:]:
            case ["-m", _]:
                entry_arguments = left_arguments[-2:]
                left_arguments = left_arguments[:-2]

            case _:
                entry_path_string = left_arguments.pop()

                _v_ = entry_path_string == "%"
                entry_arguments = [break_path_string if _v_ else entry_path_string]

        _v_ = left_arguments + [
            str(PATH / "_python_execute.py"),
            break_path_string,
            str(statement_range[0]),
            self.nvim.vvars["servername"],
        ]
        arguments = _v_ + entry_arguments + right_arguments

        try:
            _v_ = subprocess.STDOUT
            self._process = subprocess.Popen(
                arguments, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=_v_
            )

        except Exception as exception:
            self._write_error("\n".join(traceback.format_exception_only(exception)))
            return False
        #

        self._buffer_id = self.nvim.api.win_get_buf(0)

        # extmark
        _v_ = dict(right_gravity=False)
        self._extmark = self.nvim.api.buf_set_extmark(
            self._buffer_id, self._get_namespace(), statement_range[0], 0, _v_
        )

        # output
        self._create_buffer()
        self._create_window()
        threading.Thread(target=self._read_stdout, args=(self._process.stdout,)).start()

        # auto execute
        if self._autocmd_ids is None:
            self._autocmd_ids = []

            _v_ = dict(callback="_PythonExecute_CursorMoved")
            _v_ = self.nvim.api.create_autocmd(["CursorMoved", "CursorMovedI"], _v_)
            self._autocmd_ids.append(_v_)

            _v_ = dict(pattern="[niv]:[niv]", callback="_PythonExecute_ModeChanged")
            self._autocmd_ids.append(self.nvim.api.create_autocmd("ModeChanged", _v_))
        #

        self._start_args = args
        return True

    def _create_buffer(self):
        if self._output_buffer_id is not None:
            return

        self._output_buffer_id = self.nvim.api.create_buf(True, True)

    def _create_window(self):
        if self._output_window_id is not None:
            return

        _v_ = dict(cmd="split", range=[self.nvim.vars.get("pe_window_size", 8)])
        self.nvim.api.cmd(_v_, {})

        window_id = self.nvim.api.get_current_win()
        self.nvim.api.cmd(dict(cmd="wincmd", args=["p"]), {})

        self.nvim.api.win_set_buf(window_id, self._output_buffer_id)

        _v_ = str(self.nvim.funcs.win_getid(self.nvim.api.win_get_number(window_id)))
        _v_ = dict(pattern=_v_, callback="_PythonExecute_WinClosed", once=True)
        self.nvim.api.create_autocmd("WinClosed", _v_)

        self._output_window_id = window_id

    @pynvim.function("_PythonExecute_WinClosed")
    def _win_closed(self, _):
        self._output_window_id = None

    def _read_stdout(self, stdout):
        for line_bytes in stdout:
            _v_ = line_bytes.decode().removesuffix(os.linesep)
            self.nvim.async_call(self._write_line, _v_)

    def _write_line(self, line_string):
        _v_ = [line_string, ""]
        self.nvim.api.buf_set_lines(self._output_buffer_id, -2, -1, True, _v_)

        _v_ = self._output_window_id is not None
        if _v_ and self.nvim.api.get_current_win() != self._output_window_id:
            _v_ = [self.nvim.api.buf_line_count(self._output_buffer_id), 0]
            self.nvim.api.win_set_cursor(self._output_window_id, _v_)

    @pynvim.function("_PythonExecute_CursorMoved")
    def _cursor_moved(self, _):
        self._update_auto(False)

    @pynvim.function("_PythonExecute_ModeChanged")
    def _mode_changed(self, _):
        self._update_auto(True)

    def _update_auto(self, mode_changed):
        timeout_ms = self.nvim.vars.get("pe_auto_execute")

        if timeout_ms is None or self.nvim.api.win_get_buf(0) != self._buffer_id:
            if self._auto_timer is not None:
                self._auto_timer.cancel()
                self._auto_timer = None

            return

        if not mode_changed:
            position = self.nvim.api.win_get_cursor(0)
            if position == self._auto_position:
                return

            self._auto_position = position

        if self._auto_timer is not None:
            self._auto_timer.cancel()
            self._auto_timer = None

        self._auto_timer = self.nvim.loop.call_later(
            timeout_ms / 1000, self.nvim.async_call, self._execute_auto
        )

    def _execute_auto(self):
        mode_string = self._get_mode()
        if mode_string not in ("n", "i", "v"):
            return

        self.execute([mode_string], restore_selection=False)

    @pynvim.command(
        "ExecutePython",
        nargs="+",  # pyright: ignore [reportGeneralTypeIssues]
    )  # fmt: skip
    def execute(self, args, restore_selection=True):  # col: skip
        # parse arguments
        mode_string, *argument_strings = args

        if mode_string not in ("n", "i", "v"):
            self._write_error("Invalid mode")
            return

        for argument_string in argument_strings:
            name, string = argument_string.split("=")

            match name:
                case "to_stderr":
                    self._to_stderr = bool(eval(string))

                case _:
                    self._write_error(f"Unknown argument {repr(name)}")
                    return

        # get mode
        if mode_string == "v" and restore_selection:
            self.nvim.api.cmd(dict(cmd="normal", bang=True, args=["gv"]), {})

        # auto start
        if self._process is None or self._process.poll() is not None:
            if self.nvim.vars.get("pe_auto_start", 0) != 0:
                if not self.start(self._start_args):
                    return

            else:
                self.stop()
                return

        # start range
        _v_ = self._get_namespace()
        _v_ = self.nvim.api.buf_get_extmark_by_id(0, _v_, self._extmark, {})[0]
        tuple = self._get_position(_v_)

        if tuple is None:
            self._write_error("Start statement disappeared")
            return

        line_index, column_index = tuple
        start_ranges = self._get_python_execute().get_ranges(
            line_index, column_index, line_index, column_index, True
        )

        start_range = start_ranges.get("statement")
        if start_range is None:
            self._write_error("Start statement disappeared")
            return

        # selected range
        if mode_string == "v":
            _, line_number, column_number, _ = self.nvim.funcs.getpos("v")
            start_line = line_number - 1
            start_column = column_number - 1

            line_number, end_column = self.nvim.api.win_get_cursor(0)
            end_line = line_number - 1

            _v_ = end_line < start_line
            if _v_ or (start_line == end_line and end_column < start_column):
                _v_ = ((end_line, end_column), (start_line, start_column))
                (start_line, start_column), (end_line, end_column) = _v_

            end_column += 1

        else:
            line_number, start_column = self.nvim.api.win_get_cursor(0)
            start_line = line_number - 1
            end_line = start_line
            end_column = start_column + (0 if mode_string == "i" else 1)

        _v_ = start_column - (1 if mode_string == "i" else 0)
        selection_ranges = self._get_python_execute().get_ranges(
            start_line, _v_, end_line, end_column
        )

        selection_range = selection_ranges.get("statement")
        if selection_range is None:
            _v_ = mode_string == "v"
            _v_ = "No statement at selection" if _v_ else "No statement at cursor"
            self._write_error(_v_)
            return
        #

        if start_ranges["block"] != selection_ranges["block"]:
            if self.nvim.vars.get("pe_auto_scope", 0) != 0:
                if not self.start(self._start_args, line_index=selection_range[0]):
                    return

                self.execute([mode_string], restore_selection=False)

            else:
                self._write_error("Out of scope")

            return

        self._create_window()

        first_line = min(start_range[0], selection_range[0])

        _v_ = self.nvim.api.buf_get_lines(0, first_line, selection_range[2] + 1, True)
        code_bytes = "\n".join(_v_).encode()

        try:
            _v_ = typing.cast(subprocess.Popen, self._process).stdin
            stdin = typing.cast(typing.IO, _v_)

            stdin.write(struct.pack("<i", len(code_bytes)))
            stdin.write(code_bytes)

            _v_ = struct.pack(
                "<iiiic?",
                start_line - first_line,
                start_column,
                end_line - first_line,
                end_column,
                mode_string.encode(),
                self._to_stderr,
            )
            stdin.write(_v_)

            stdin.flush()

        except BrokenPipeError:
            self.stop()

    @pynvim.function("_PythonExecute_ShowObject")
    def _show_object(self, args):
        (dictionary,) = args

        self._close_window()  # Warning: assumes all events are processed immediately

        _v_ = [f" {line_string} " for line_string in self._get_line_strings(dictionary)]
        line_strings = _v_

        self._window_buffer_id = self.nvim.api.create_buf(False, True)
        self.nvim.api.buf_set_lines(self._window_buffer_id, 0, -1, False, line_strings)
        self.nvim.api.buf_set_option(self._window_buffer_id, "readonly", True)
        self.nvim.api.buf_set_option(self._window_buffer_id, "filetype", "python")

        line_index = self.nvim.funcs.winline() - 1
        column_index = self.nvim.funcs.wincol() - 1
        window_width = self.nvim.api.win_get_width(0)
        window_height = self.nvim.api.win_get_height(0)

        _v_ = max(1, min(max(map(len, line_strings)), window_width - column_index - 2))
        _v_ = dict(
            relative="cursor",
            row=1,
            col=0,
            width=_v_,
            height=max(1, min(len(line_strings), window_height - line_index - 3)),
            style="minimal",
            border="rounded",
        )
        _v_ = self.nvim.api.open_win(self._window_buffer_id, False, _v_)
        self._window_window_id = _v_

        _v_ = dict(callback="_PythonExecute_CursorMoved2")
        _v_ = self.nvim.api.create_autocmd(["CursorMoved", "CursorMovedI"], _v_)
        self._window_autocmd_id = _v_

        # necessary because user might close the window manually
        _v_ = self.nvim.api.win_get_number(self._window_window_id)
        _v_ = str(self.nvim.funcs.win_getid(_v_))
        _v_ = dict(pattern=_v_, callback="_PythonExecute_WinClosed2", once=True)
        self.nvim.api.create_autocmd("WinClosed", _v_)

    def _get_line_strings(self, dictionary):
        repr_string = dictionary.get("repr")
        if repr_string is not None:
            yield repr_string

        if dictionary.get("repr_incomplete", False):
            yield "..."

        for name, score, repr_string, incomplete in dictionary.get("attributes", []):
            score_string = "" if score is None else f"{int(score):3} "
            incomplete_string = "..." if incomplete else ""
            yield f"{score_string}.{name} = {repr_string}{incomplete_string}"

        if dictionary.get("attributes_incomplete", False):
            yield "..."

        parameters = dictionary.get("parameters")
        if parameters is not None:
            yield "("

            if parameters == "?":
                yield "    ?"

            else:
                for parameter, score in parameters:
                    score_string = "" if score is None else f"{int(score):3} "
                    yield f"    {score_string}{parameter},"

                if dictionary["parameters_incomplete"]:
                    yield "    ..."

            yield ")"

    @pynvim.function("_PythonExecute_CursorMoved2")
    def _cursor_moved_2(self, _):
        if self.nvim.api.get_current_win() == self._window_window_id:
            return

        self._close_window()

    @pynvim.function("_PythonExecute_WinClosed2")
    def _win_closed_2(self, _):
        self._window_window_id = None
        self._close_window()

    def _close_window(self):
        if self._window_autocmd_id is not None:
            self.nvim.api.del_autocmd(self._window_autocmd_id)
            self._window_autocmd_id = None

        if self._window_window_id is not None:
            window_id = self._window_window_id
            self._window_window_id = None

            self.nvim.api.win_close(window_id, True)

        if self._window_buffer_id is not None:
            buffer_id = self._window_buffer_id
            self._window_buffer_id = None

            self.nvim.api.buf_delete(buffer_id, dict(force=True))

    def _get_position(self, line_index):
        view = self.nvim.funcs.winsaveview()

        self.nvim.api.win_set_cursor(0, [line_index + 1, 0])
        line_number, column_number = self.nvim.funcs.searchpos(r"\v\S", "cn")

        self.nvim.funcs.winrestview(view)

        if line_number == 0 and column_number == 0:
            return None

        return (line_number - 1, column_number - 1)

    @pynvim.command("StopPython")
    def stop(self, as_error=True):
        if self._process is not None:
            if self._process.poll() is None:
                try:
                    stdin = typing.cast(typing.IO, self._process.stdin)
                    stdin.write(struct.pack("<i", 0))
                    stdin.flush()

                except BrokenPipeError:
                    pass

            try:
                return_code = self._process.wait(5)

            except subprocess.TimeoutExpired:
                self._write_error("Process didn't stop", as_error=as_error)
                return

            else:
                if return_code != 0:
                    _v_ = f"Process stopped with return code {return_code}"
                    self._write_error(_v_, as_error=as_error)

                self._process = None

        if self._extmark is not None:
            _v_ = self._get_namespace()
            self.nvim.api.buf_del_extmark(self._buffer_id, _v_, self._extmark)

            self._extmark = None

    def _write_error(self, string, as_error=True):
        _v_ = not as_error or self._get_mode() in ("i", "v", "V", "\x16")
        write = self.nvim.api.out_write if _v_ else self.nvim.api.err_write

        write(string)
        if not string.endswith("\n"):
            write("\n")

    def _get_mode(self):
        return self.nvim.funcs.mode()  # api.get_mode() freezes for some reason

    def _get_namespace(self):
        return self.nvim.api.create_namespace("PythonExecute")

    def _get_python_execute(self):
        if self._python_execute is not None:
            return self._python_execute

        self.nvim.exec_lua("""
local function get_ranges(start_line, start_column, end_line, end_column, skip_comments)
    local ts_node = vim.treesitter.get_parser(0, 'python'):named_node_for_range({start_line, start_column, end_line, end_column})
    if ts_node == nil then
        return {_=0}  -- _ ensures type dict
    end
    local part_range = {ts_node:range(false)}
    local previous_node = nil
    while true do
        local type = ts_node:type()
        if type == 'block' or type == 'module' then
            local statement_range = nil
            if skip_comments then
                while previous_node ~= nil and previous_node:type() == 'comment' do
                    previous_node = previous_node:next_sibling()
                end
            end
            if previous_node ~= nil and previous_node:type() ~= 'comment' then
                statement_range = {previous_node:range(false)}
            end
            return {part=part_range, statement=statement_range, block={ts_node:range(false)}}
        end
        previous_node = ts_node
        ts_node = ts_node:parent()
    end
end

_python_execute = {get_ranges=get_ranges, test=test}
""")
        self._python_execute = self.nvim.lua._python_execute
        return self._python_execute
