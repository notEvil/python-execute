import libcst
import libcst.helpers as l_helpers
import libcst.metadata as l_metadata
import pynvim
import rapidfuzz.process as r_process
import bdb
import collections.abc as c_abc
import dataclasses
import datetime
import inspect
import pathlib
import re
import runpy
import struct
import sys
import tempfile
import time
import traceback
import typing


def _lt_CodePosition(first_position, second_position):
    return first_position.line < second_position.line or (
        first_position.line == second_position.line
        and first_position.column < second_position.column
    )


def _le_CodePosition(first_position, second_position):
    _v_ = first_position == second_position
    return _v_ or _lt_CodePosition(first_position, second_position)


class _IGNORE:
    pass


class _Main:
    def __init__(self):
        super().__init__()

        self._nvim = None
        self._start_time = None
        self._temp_file = None

    def __call__(self):
        # arguments
        break_path_string, line_index_string, servername_string = sys.argv[1:4]
        break_path = pathlib.Path(break_path_string)
        line_index = int(line_index_string)
        del sys.argv[:4]
        #

        self._nvim = pynvim.attach("socket", path=servername_string)

        bdb = _Bdb(self)
        bdb.set_break(bdb.canonic(str(break_path)), line_index + 1)

        with tempfile.NamedTemporaryFile() as self._temp_file:
            _print_stderr()
            _print_stderr(f"Starting ({datetime.datetime.now().isoformat()})")

            self._start_time = time.monotonic()
            bdb.runcall(self._function)

        _print_stderr("! Process ended !")
        _print_stderr()

    def _function(self):
        if sys.argv[0] == "-m":
            sys.argv.pop(0)
            runpy.run_module(sys.argv[0], run_name="__main__", alter_sys=True)

        else:
            runpy.run_path(sys.argv[0], run_name="__main__")


class _Bdb(bdb.Bdb):
    def __init__(self, main):
        super().__init__()

        self.main = main

    def user_line(self, frame):
        if not self.break_here(frame):
            self.set_continue()
            return

        seconds = time.monotonic() - self.main._start_time

        _v_ = f"{int(seconds * 1e3)} ms" if seconds < 0.1 else f"{seconds:0.1f} s"
        _print_stderr("Started (", _v_, ")", sep="")

        _print_stderr()

        _process_requests(frame, self, self.main)


@dataclasses.dataclass
class _Request:
    code_string: str
    start_line: int
    start_column: int
    end_line: int
    end_column: int
    mode_string: str
    to_stderr: bool


def _process_requests(frame, bdb, main):
    globals_dict = dict(frame.f_globals)
    globals_dict.update(frame.f_locals)

    while True:
        sys.stdout.flush()
        sys.stderr.flush()

        (length,) = struct.unpack("<i", sys.stdin.buffer.read(4))

        if length == 0:
            bdb.set_quit()
            break

        code_bytes = sys.stdin.buffer.read(length)

        _v_ = struct.unpack("<iiiic?", sys.stdin.buffer.read(18))
        start_line, start_column, end_line, end_column, mode_bytes, to_stderr = _v_

        _v_ = _Request(
            code_string=code_bytes.decode(),
            start_line=start_line,
            start_column=start_column,
            end_line=end_line,
            end_column=end_column,
            mode_string=mode_bytes.decode(),
            to_stderr=to_stderr,
        )
        _process_request(_v_, dict(globals_dict), main)


def _process_request(request, globals_dict, main):
    # parse code
    tuple = _parse_code(request)
    if tuple is None:
        if request.to_stderr:
            _print_stderr()
        return

    metadata_wrapper, cst_nodes, first_line = tuple

    # get part
    tuple = _get_part(metadata_wrapper, cst_nodes, first_line, request)
    if tuple is None:
        if request.to_stderr:
            _print_stderr()
        return

    part_node, cst_visitor, cst_nodes = tuple
    #

    parent_nodes = None

    def get_parent_nodes():
        nonlocal parent_nodes

        if parent_nodes is None:
            parent_nodes = metadata_wrapper.resolve(l_metadata.ParentNodeProvider)

        return parent_nodes

    # get insert
    tuple = _get_insert(part_node, get_parent_nodes, request)
    if tuple is None:
        if request.to_stderr:
            _print_stderr()
        return

    part_node, insert_string, insert_name = tuple

    # get link
    part_node = _get_link(part_node, cst_visitor)
    if part_node is None:
        if request.to_stderr:
            _print_stderr()
        return

    # get raise
    _v_ = _get_raise(part_node, get_parent_nodes, cst_visitor, cst_nodes, globals_dict)
    part_node, cst_nodes, globals_dict = _v_
    #

    if isinstance(part_node, libcst.BaseExpression):
        all = part_node in cst_visitor._target_nodes and request.mode_string != "i"
        if len(cst_nodes) != 1 or all:
            _v_ = libcst.Module(body=cst_nodes if all else cst_nodes[:-1]).code
            _v_ = _except(exec)(_compile_string(_v_, "exec", main), globals_dict)
            if _v_ is _Failed:
                if request.to_stderr:
                    _print_stderr()
                return

        sys.stdout.flush()
        sys.stderr.flush()

        _v_ = [libcst.SimpleStatementLine(body=[libcst.Expr(value=part_node)])]
        _v_ = _compile_string(libcst.Module(body=_v_).code, "eval", main)
        object = _except(eval)(_v_, globals_dict)

        sys.stdout.flush()
        sys.stderr.flush()

        if object not in (None, _Failed):
            _show_object(object, insert_string, insert_name, request, main)

    else:
        _v_ = _compile_string(libcst.Module(body=cst_nodes).code, "exec", main)
        object = _except(exec)(_v_, globals_dict)

        if object not in (None, _Failed):
            _show_object(object, insert_string, insert_name, request, main)

    if request.to_stderr:
        _print_stderr()


def _parse_code(request):
    match = re.search(r"^(?P<indent>\s*)\S", request.code_string, re.MULTILINE)
    if match is None or len(match.group("indent")) == 0:
        _v_ = _except(libcst.parse_module, exception_only=True)(request.code_string)
        module_node = _v_
        if module_node is _Failed:
            return

        module_node = typing.cast(libcst.Module, module_node)
        metadata_wrapper = l_metadata.MetadataWrapper(module_node)

        return (metadata_wrapper, metadata_wrapper.module.body, 0)

    else:
        request.code_string = f"def _():\n{request.code_string}"
        request.start_line += 1
        request.end_line += 1

        _v_ = _except(libcst.parse_module, exception_only=True)(request.code_string)
        module_node = _v_
        if module_node is _Failed:
            return

        module_node = typing.cast(libcst.Module, module_node)
        metadata_wrapper = l_metadata.MetadataWrapper(module_node)

        _v_ = typing.cast(libcst.FunctionDef, metadata_wrapper.module.body[0]).body.body
        cst_nodes = _v_  # Module, FunctionDef, IndentedBlock

        return (metadata_wrapper, cst_nodes, 1)


def _get_part(metadata_wrapper, cst_nodes, first_line, request):
    code_ranges = metadata_wrapper.resolve(l_metadata.PositionProvider)

    _v_ = request.start_column - (1 if request.mode_string == "i" else 0)
    code_range = l_metadata.CodeRange(
        start=l_metadata.CodePosition(line=request.start_line + 1, column=_v_),
        end=l_metadata.CodePosition(
            line=request.end_line + 1, column=request.end_column
        ),
    )

    def is_in(cst_node):
        node_range = code_ranges[cst_node]

        if hasattr(cst_node, "lpar"):
            _v_ = cst_node.rpar is not None and len(cst_node.rpar) != 0
            node_range = l_metadata.CodeRange(
                start=(
                    code_ranges[cst_node.lpar[0]].start
                    if cst_node.lpar is not None and len(cst_node.lpar) != 0
                    else node_range.start
                ),
                end=(code_ranges[cst_node.rpar[-1]].end if _v_ else node_range.end),
            )

        _v_ = _le_CodePosition(node_range.start, code_range.start)
        return _v_ and _le_CodePosition(code_range.end, node_range.end)

    cst_visitor = _CSTVisitor(is_in)
    cst_nodes[-1].visit(cst_visitor)

    part_node = cst_visitor._part_node

    _v_ = part_node is None or code_ranges[part_node].start.line <= first_line
    if _v_ or (request.mode_string == "i" and part_node in cst_visitor._name_nodes):
        return

    if isinstance(cst_visitor._outer_node, libcst.BaseExpression):
        cst_nodes = list(cst_nodes)

        _v_ = [libcst.Expr(value=cst_visitor._outer_node)]
        cst_nodes[-1] = libcst.SimpleStatementLine(body=_v_)

    return (part_node, cst_visitor, cst_nodes)


class _CSTVisitor(libcst.CSTVisitor):
    def __init__(self, is_in):
        super().__init__()

        self.is_in = is_in

        self._part_node = None
        self._part_raise = None
        self._outer_node = None

        self._links = {}  # {from~libcst.CSTNode: to~libcst.CSTNode}
        self._target_nodes = set()  # nodes that are targets of (implicit) assignments
        self._name_nodes = set()  # nodes that are just names

        self._raise_nodes = set()
        self._raise = False
        self._raises = []

    def on_visit(self, cst_node):
        if cst_node in self._raise_nodes:
            self._raises.append(self._raise)
            self._raise = True

        if self._part_node is not None:
            return False

        return super().on_visit(cst_node)

    def on_leave(self, cst_node):
        object = super().on_leave(cst_node)

        if self._set_discard(self._raise_nodes, cst_node):
            self._raise = self._raises.pop()

        return object

    def _set_discard(self, set, object):
        length = len(set)
        set.discard(object)
        return len(set) != length

    def visit_Attribute(self, cst_node):
        self._links[cst_node.attr] = cst_node

    def visit_Arg(self, cst_node):
        if cst_node.keyword is not None:
            self._links[cst_node.keyword] = cst_node.value

    def _visit_assign(self, node):
        cst_node = node
        self._visit_assign_target(cst_node.target)

    visit_AnnAssign = _visit_assign
    visit_AugAssign = _visit_assign
    visit_AssignTarget = _visit_assign

    def _visit_def(self, node):
        cst_node = node
        self._target_nodes.add(cst_node.name)
        self._name_nodes.add(cst_node.name)

    visit_ClassDef = _visit_def
    visit_FunctionDef = _visit_def

    def visit_ExceptHandler(self, cst_node):
        if cst_node.name is not None:
            self._links[cst_node.name.name] = cst_node.type
            self._name_nodes.add(cst_node.name.name)

    def visit_Import(self, cst_node):
        for alias_node in cst_node.names:
            aliased = alias_node.asname is not None

            if aliased:
                self._target_nodes.add(alias_node.asname.name)
                self._name_nodes.add(alias_node.asname.name)

            for path_node in self._get_nodes(alias_node.name):
                if aliased:
                    self._links[path_node] = None

                else:
                    self._target_nodes.add(path_node)

                self._name_nodes.add(path_node)
                if isinstance(path_node, libcst.Attribute):
                    self._name_nodes.add(path_node.attr)

    def visit_ImportFrom(self, cst_node):
        if cst_node.module is not None:
            for path_node in self._get_nodes(cst_node.module):
                self._links[path_node] = None

                self._name_nodes.add(path_node)
                if isinstance(path_node, libcst.Attribute):
                    self._name_nodes.add(path_node.attr)

        if isinstance(cst_node.names, libcst.ImportStar):
            return

        for alias_node in cst_node.names:
            if alias_node.asname is None:
                self._target_nodes.add(alias_node.name)

            else:
                self._links[alias_node.name] = None
                self._target_nodes.add(alias_node.asname.name)
                self._name_nodes.add(alias_node.asname.name)

    def _get_nodes(self, cst_node):
        while True:
            yield cst_node

            if not isinstance(cst_node, libcst.Attribute):
                break

            cst_node = cst_node.value

    def visit_NameItem(self, node):
        return False

    def visit_Param(self, cst_node):
        if cst_node.default is None:
            self._links[cst_node.name] = _IGNORE

        else:
            self._links[cst_node.name] = cst_node.default
            self._name_nodes.add(cst_node.name)

    def visit_WithItem(self, cst_node):
        if cst_node.asname is not None:
            self._links[cst_node.asname.name] = cst_node.item
            self._name_nodes.add(cst_node.asname.name)

    def visit_IfExp(self, cst_node):
        self._raise_nodes.update([cst_node.body, cst_node.orelse])

    def visit_Lambda(self, cst_node):
        self._raise_nodes.add(cst_node.body)

    def _visit_comprehension(self, node):
        cst_node = node
        self._raise_nodes.add(cst_node.elt)
        self._add_comp_for(cst_node.for_in)

    visit_GeneratorExp = _visit_comprehension
    visit_ListComp = _visit_comprehension
    visit_SetComp = _visit_comprehension

    def visit_DictComp(self, cst_node):
        self._raise_nodes.update([cst_node.key, cst_node.value])
        self._add_comp_for(cst_node.for_in)

    def _add_comp_for(self, comp_for_node):
        self._visit_assign_target(comp_for_node.target)
        self._raise_nodes.add(comp_for_node.target)

        self._raise_nodes.add(comp_for_node.iter)
        self._raise_nodes.update(comp_for_node.ifs)

        if comp_for_node.inner_for_in is not None:
            self._add_comp_for(comp_for_node.inner_for_in)

    def visit_For(self, cst_node):
        self._visit_assign_target(cst_node.target)
        self._raise_nodes.add(cst_node.target)

    def _visit_assign_target(self, cst_node):
        if isinstance(cst_node, (libcst.Tuple, libcst.List)):
            self._links[cst_node] = None

            for element_node in cst_node.elements:
                self._visit_assign_target(element_node.value)

        else:
            self._target_nodes.add(cst_node)

    def visit_If(self, cst_node):
        if isinstance(cst_node.orelse, libcst.If):
            self._raise_nodes.add(cst_node.orelse.test)

    def _leave(self, original_node):
        cst_node = original_node
        self._outer_node = cst_node

        _v_ = self._part_node is None and self.is_in(cst_node)
        if not (_v_ and self._follow_links(cst_node) is not None):
            return

        self._part_node = cst_node
        self._part_raise = self._raise

    def _follow_links(self, cst_node):
        while cst_node in self._links:
            cst_node = self._links[cst_node]

        return cst_node

    leave_Name = _leave
    leave_Attribute = _leave
    leave_UnaryOperation = _leave
    leave_BinaryOperation = _leave
    leave_BooleanOperation = _leave
    leave_Comparison = _leave
    leave_IfExp = _leave
    leave_Lambda = _leave
    leave_Call = _leave
    leave_Integer = _leave
    leave_Float = _leave
    leave_Imaginary = _leave
    leave_SimpleString = _leave
    leave_ConcatenatedString = _leave
    leave_FormattedString = _leave
    leave_Tuple = _leave
    leave_List = _leave
    leave_Set = _leave
    leave_Dict = _leave
    leave_GeneratorExp = _leave
    leave_ListComp = _leave
    leave_SetComp = _leave
    leave_DictComp = _leave
    leave_Subscript = _leave
    leave_AnnAssign = _leave
    leave_Assert = _leave
    leave_Assign = _leave
    leave_AugAssign = _leave
    leave_Del = _leave
    leave_Import = _leave
    leave_ImportFrom = _leave
    leave_Pass = _leave
    leave_Raise = _leave
    leave_ClassDef = _leave
    leave_For = _leave
    leave_FunctionDef = _leave
    leave_If = _leave
    leave_Try = _leave
    leave_While = _leave
    leave_With = _leave


def _get_insert(part_node, get_parent_nodes, request):
    default_tuple = (part_node, None, None)

    if request.mode_string != "i":
        return default_tuple

    match = re.search(
        r"""
(?P<type>
    \.  # attribute
  | [(,]\s*  # call
  | (^\s*from\s+(?P<dots>\.+)?(?P<module>\w+(\.\w+)*)\s+import\s+(\w+\s*,\s*)*)  # from import
)
(?P<stars>[*]{1,2}\s*)?
((?P<name>[a-zA-Z_]\w*)(?P<equal>\s*=\s*)?)?
$
""",
        request.code_string.split("\n")[request.start_line][: request.start_column],
        re.VERBOSE,
    )
    if match is None:
        return default_tuple

    type_string, dots_string, module_string, stars_string, insert_name, equal_string = (
        match.group("type", "dots", "module", "stars", "name", "equal")
    )
    insert_string = type_string.lstrip()[:1]

    if insert_string == ".":
        if stars_string is not None or equal_string is not None:
            return default_tuple

        if insert_name is None:  # `.|`
            if not isinstance(part_node, libcst.Attribute):
                return default_tuple

            part_node = part_node.value

        else:  # `.{name}|`
            if not isinstance(part_node, libcst.Name):
                return default_tuple

            part_node = get_parent_nodes()[part_node].value

    elif insert_string in ("(", ","):
        if stars_string is not None and insert_name is not None:
            return default_tuple

        _v_ = insert_name is None or equal_string is not None
        if _v_:  # `({ws}|` or `,{ws}|` or `({ws}{name}=|` or `,{ws}{name}=|`
            if not isinstance(part_node, libcst.Call):
                return default_tuple

            part_node = part_node.func

        else:  # `({ws}{name}|` or `,{ws}{name}|`
            cst_node = part_node
            for type in [libcst.Arg, libcst.Call]:
                cst_node = get_parent_nodes()[cst_node]
                if not isinstance(cst_node, type):
                    return default_tuple

            part_node = cst_node.func

    elif insert_string == "f":
        if dots_string is not None or stars_string is not None:
            return default_tuple

        if insert_name is None:  # `from{ws}{module}{ws}import{ws}|`
            if not isinstance(part_node, libcst.ImportFrom):
                return default_tuple

            part_node = _get_import_node(module_string)

        else:  # `from{ws}{module}{ws}import{ws}{name}|`
            if not isinstance(part_node, libcst.Name):
                return default_tuple

            part_node = _get_import_node(module_string)

    return (part_node, insert_string, insert_name)


def _get_import_node(string):
    return libcst.parse_expression(f"__import__({repr(string)}, fromlist=[''])")


def _get_link(part_node, cst_visitor):
    while True:
        link_node = cst_visitor._links.get(part_node)
        if link_node is None:
            break

        if link_node is _IGNORE:
            return

        part_node = link_node

    return part_node


class _Raised(Exception):
    pass


def _get_raise(part_node, get_parent_nodes, cst_visitor, cst_nodes, globals_dict):
    if not cst_visitor._part_raise:
        return (part_node, cst_nodes, globals_dict)

    cst_nodes = list(cst_nodes)

    if part_node in cst_visitor._target_nodes:
        cst_node = part_node
        while True:
            cst_node = get_parent_nodes()[cst_node]
            if not isinstance(cst_node, libcst.Element):
                parent_node = cst_node
                break

            cst_node = get_parent_nodes()[cst_node]

        if isinstance(parent_node, libcst.For):
            assert parent_node is cst_nodes[-1]

            _v_ = "__python_execute_raise__({part_node})"
            _v_ = l_helpers.parse_template_statement(_v_, part_node=part_node)
            part_node = parent_node.deep_replace(parent_node.body.body[0], _v_)

        elif isinstance(parent_node, libcst.CompFor):
            _v_ = "__python_execute_raise__({part_node})"
            _v_ = l_helpers.parse_template_expression(_v_, part_node=part_node)
            _v_ = [libcst.CompIf(test=_v_)]
            _v_ = libcst.CompFor(
                target=parent_node.target, iter=parent_node.iter, ifs=_v_
            )
            part_node = cst_nodes[-1].deep_replace(parent_node, _v_)

        else:
            raise Exception

    else:
        _v_ = "__python_execute_raise__({part_node})"
        _v_ = l_helpers.parse_template_expression(_v_, part_node=part_node)
        part_node = cst_nodes[-1].deep_replace(part_node, _v_)

    cst_nodes[-1] = part_node

    globals_dict = dict(globals_dict)
    globals_dict["__python_execute_raise__"] = _raise_object

    return (part_node, cst_nodes, globals_dict)


def _raise_object(object):
    raise _Raised(object)


def _compile_string(string, mode, main):
    main._temp_file.seek(0)
    main._temp_file.write(string.encode())
    main._temp_file.write(b"\n")
    main._temp_file.flush()
    return compile(string, main._temp_file.name, mode)


def _show_object(object, insert_string, insert_name, request, main):
    dictionary = _get_dictionary(object, insert_string, insert_name, request, main)

    if request.to_stderr:
        for line_string in _get_line_strings(dictionary):
            _print_stderr(line_string)

    else:
        main._nvim.funcs._PythonExecute_ShowObject(dictionary)


def _get_dictionary(object, insert_string, insert_name, request, main):
    if request.mode_string != "i":
        return dict(repr=repr(object))

    _v_ = insert_string == "." and insert_name is not None
    if _v_ and hasattr(object, insert_name):
        object = getattr(object, insert_name)
        if object is not None:
            return _get_dictionary(object, None, None, request, main)

        return {}

    dictionary = {}

    line_strings = repr(object).split("\n")
    incomplete = 10 < len(line_strings)

    dictionary["repr"] = "\n".join(line_strings[: 9 if incomplete else 10])
    dictionary["repr_incomplete"] = incomplete

    _v_ = inspect.isfunction(object) or inspect.ismethod(object)
    if not (_v_ or insert_string in ("(", ",")):
        incomplete = False

        if insert_name is None:
            tuples = [(name, None) for name in dir(object) if not name.startswith("_")]

        else:
            names = [name for name in dir(object) if not name.startswith("__")]
            length = len(names)

            _v_ = r_process.extract(insert_name, names, processor=str.lower, limit=10)
            tuples = [(name, score) for name, score, _ in _v_ if score != 0]

            incomplete = len(tuples) != length

        attribute_tuples = []

        for name, score in tuples:
            line_strings = _repr_object(getattr(object, name)).split("\n")

            _v_ = (name, score, line_strings[0], 1 < len(line_strings))
            attribute_tuples.append(_v_)

        dictionary["attributes"] = attribute_tuples
        dictionary["attributes_incomplete"] = incomplete

    if isinstance(object, c_abc.Callable) and insert_string != ".":
        try:
            signature = inspect.signature(object)

        except (TypeError, ValueError):
            dictionary["parameters"] = "?"

        else:
            incomplete = False

            if insert_name is None:
                _v_ = [(parameter, None) for parameter in signature.parameters.values()]
                tuples = _v_

            else:
                length = len(signature.parameters)

                _v_ = signature.parameters.values()
                _v_ = {parameter: parameter.name for parameter in _v_}
                _v_ = r_process.extract(insert_name, _v_, processor=str.lower, limit=10)
                _v_ = [(parameter, score) for _, score, parameter in _v_ if score != 0]
                tuples = _v_

                incomplete = len(tuples) != length

            _v_ = [(_repr_parameter(parameter), score) for parameter, score in tuples]
            dictionary["parameters"] = _v_

            dictionary["parameters_incomplete"] = incomplete

    return dictionary


def _repr_object(object):
    _v_ = inspect.isfunction(object) or inspect.ismethod(object)
    _v_ = _v_ or inspect.isbuiltin(object)
    if _v_ or type(object).__name__ == "cython_function_or_method":
        code_object = getattr(object, "__code__", None)
        name = object.__name__ if code_object is None else code_object.co_name

        try:
            signature = inspect.signature(object)

        except ValueError:
            return repr(object)

        parameters_string = "".join(_repr_parameters(signature.parameters.values()))

        _v_ = signature.return_annotation == inspect._empty
        _v_ = "" if _v_ else f" -> {_repr_annotation(signature.return_annotation)}"
        annotation_string = _v_

        return f"{name}({parameters_string}){annotation_string}"

    return repr(object)


def _repr_parameters(parameters):
    pos_only = False

    previous_parameter = None
    for parameter in parameters:
        if previous_parameter is not None:
            yield ", "

        if parameter.kind == inspect.Parameter.POSITIONAL_ONLY:
            pos_only = True

        else:
            if pos_only:
                yield "/, "
                pos_only = False

        yield _repr_parameter(parameter)

        previous_parameter = parameter

    if pos_only:
        yield ", /"


def _repr_parameter(parameter):
    _v_ = {inspect.Parameter.VAR_POSITIONAL: "*", inspect.Parameter.VAR_KEYWORD: "**"}
    prefix_string = _v_.get(parameter.kind, "")

    has_annotation = parameter.annotation != inspect._empty

    _v_ = f": {_repr_annotation(parameter.annotation)}" if has_annotation else ""
    annotation_string = _v_

    space_string = " " if has_annotation else ""

    _v_ = parameter.default == inspect._empty
    default_string = (
        "" if _v_ else f"{space_string}={space_string}{_repr_object(parameter.default)}"
    )

    return f"{prefix_string}{parameter.name}{annotation_string}{default_string}"


def _repr_annotation(object):
    if type(object) is type:
        return object.__qualname__

    if type(object) is str:
        return repr(object)

    return str(object)


def _get_line_strings(dictionary):
    repr_string = dictionary.get("repr")
    if repr_string is not None:
        yield repr_string

    if dictionary.get("repr_incomplete", False):
        yield "..."

    for name, score, repr_string, incomplete in dictionary.get("attributes", []):
        score_string = "" if score is None else f"{int(score):3} "
        incomplete_string = "..." if incomplete else ""
        yield f"{score_string}.{name} = {repr_string}{incomplete_string}"

    if dictionary.get("attributes_incomplete", False):
        yield "..."

    parameters = dictionary.get("parameters")
    if parameters is not None:
        yield "("

        if parameters == "?":
            yield "    ?"

        else:
            for parameter, score in parameters:
                score_string = "" if score is None else f"{int(score):3} "
                yield f"    {score_string}{parameter}"

            if dictionary["parameters_incomplete"]:
                yield "    ..."

        yield ")"


def _except(function, exception_only=False):
    def _except(*args, **kwargs):
        try:
            return function(*args, **kwargs)

        except _Raised as raised:
            (object,) = raised.args
            return object

        except Exception as exception:
            if exception_only:
                _print_stderr("\n".join(traceback.format_exception_only(exception)))

            else:
                traceback.print_exc(file=sys.stderr)

        return _Failed

    return _except


class _Failed:
    pass


def _print_stderr(*args, **kwargs):
    sys.stdout.flush()
    print(*args, file=sys.stderr, **kwargs)


if __name__ == "__main__":
    _Main()()
