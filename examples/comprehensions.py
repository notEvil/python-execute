[
    (a + b, c * d)
    for a in range(4)
    if 0 < a
    for b, (c, d) in [(a + 1, (a + 2, a + 3))]
    if d % a == 0
]
