import rapidfuzz.process as r_process


def get_best_matches(query, choices, n=5, min_score=50):
    return [
        choice
        for _, (choice, score, _) in zip(range(n), r_process.extract(query, choices))
        if min_score <= score
    ]


if __name__ == "__main__":
    import sys

    query, *choices = sys.argv[1:]

    get_best_matches(query, choices)
